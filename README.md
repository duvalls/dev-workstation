# dev-workstation
1. Install VirtualBox - https://www.oracle.com/virtualization/technologies/vm/downloads/virtualbox-downloads.html?source=:ow:o:p:nav:mmddyyVirtualBoxHero&intcmp=:ow:o:p:nav:mmddyyVirtualBoxHero
1. Install Git - https://git-scm.com/download/win
1. Install Vagrant - https://www.vagrantup.com/downloads
1. Clone the repo
1. Update configuration if necessary
1. Run `vagrant up`

## This VM includes VSCode